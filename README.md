# Personal slock build
includes patches for:

1. pam - https://tools.suckless.org/slock/patches/pam_auth/

2. messages - https://tools.suckless.org/slock/patches/pam_auth/

# pam hints
set user and group  in config.h to something that can use pam ($USER or root)

for fingerprint add: ``auth      sufficient pam_fprintd.so`` to ``/etc/pam.d/system-local-login``

The following is nice in ``/etc/pam.d/sudo``. Allows to use password or fingerprint

```
auth required    pam_env.so
auth sufficient  pam_unix.so try_first_pass likeauth nullok
auth sufficient  pam_fprintd.so
```
